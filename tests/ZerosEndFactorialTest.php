<?php
use PHPUnit\Framework\TestCase;

class ZerosEndFactorialTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCanBeConstructInt()
    {
        new ZerosEndFactorial(1);
    }

    /**
     * @throws Exception
     */
    public function testCanBeConstructStrInt()
    {
        $this->expectException(Exception::class);
        new ZerosEndFactorial('1');
    }

    /**
     * @throws Exception
     */
    public function testCanBeConstructStrChar()
    {
        $this->expectException(Exception::class);
        new ZerosEndFactorial('sadf');
    }

    /**
     * @throws Exception
     */
    public function testCanBeCorrectCount()
    {
        $class = new ZerosEndFactorial(100);
        $this->assertEquals(24,$class->getCount());
    }

    /**
     * @throws Exception
     */
    public function testCanNotBeCorrectCount()
    {
        $class = new ZerosEndFactorial(100);
        $this->assertNotEquals(20,$class->getCount());
    }
}
