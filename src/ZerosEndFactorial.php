<?php

/**
 * Class ZerosEndFactorial
 * How many zeros at the end of factorial
 */
class ZerosEndFactorial
{
    /**
     * @var int
     */
    protected $factorial;

    /**
     * @var array
     */
    protected $dividers = [2, 5];

    /**
     * ZerosEndFactorial constructor.
     * @param $factorial
     * @throws \Exception
     */
    public function __construct($factorial)
    {
        $this->validate($factorial);
        $this->factorial = $factorial;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $result = 0;
        while ($this->getEfficient($this->factorial) > 0) {
            $this->factorial = $this->getEfficient($this->factorial);
            $result += $this->factorial;
        }
        return $result;
    }

    /**
     * @param $number
     * @return int
     */
    protected function getEfficient($number)
    {
        foreach ($this->dividers as $divider) {
            $result[] = floor($number / $divider);
        }
        sort($result);
        return array_shift($result);
    }

    /**
     * @param $number
     * @throws \Exception
     */
    protected function validate($number)
    {
        if (!is_int($number)) {
            throw new \Exception("Input {$number} is not integer");
        }
    }
}